#!/usr/bin/env python
# coding: utf-8

import cPickle as pickle
import simplekml

DOC_ROOT = 'http://stredovek.com/'
STYLES = {
    'hradiste': {'title': u'Hradiště', 'color': 'ffffffff', 'icon': 'http://maps.google.com/mapfiles/ms/micons/blue.png'},
    'hrady': {'title': u'Hrady', 'color': 'ffffffff', 'icon': 'http://maps.google.com/mapfiles/ms/micons/red.png'},
    'rozhledny': {'title': u'Rozhledny', 'color': 'ffffffff', 'icon': 'http://maps.google.com/mapfiles/ms/micons/orange.png'},
    'jine': {'title': u'Ostatní', 'color': 'ffffffff', 'icon': 'http://maps.google.com/mapfiles/ms/micons/yellow.png'},
}


with open('places.pkl', 'rb') as f:
    places = pickle.load(f)

#import pprint
#pprint.pprint(places)


def format_desc(category, place):
    desc = ['<em>', STYLES[category]['title'], '</em> | ', DOC_ROOT + place['url'], '<br></br>']
    desc.append('<div style="min-height: 113px;">')
    for img in place['images'][:3]:
        desc.append('<img src="%s%s">' % (DOC_ROOT, img[0]))
    desc.append('</div>')
    desc.append(place['desc'])
    return ''.join(desc)

kml = simplekml.Kml()
for (category, places) in places.iteritems():
    folder = kml.newfolder(name=STYLES[category]['title'])
    style = simplekml.Style()
    icon = simplekml.Icon(href=STYLES[category]['icon'])
    color = STYLES[category]['color']
    style.iconstyle = simplekml.IconStyle(icon=icon, color=color)
    for place in places:
        url = place['url'].replace('&', '&amp;')
        point = folder.newpoint(
            name=place['title'],
            coords=[(place['lon'], place['lat'])],
            description=format_desc(category, place),
            #atomlink=DOC_ROOT+url,
        )
        point.style = style

kml.document.open = 1
kml.document.lookat = simplekml.LookAt(range=220000, tilt=15, longitude=14.55, latitude=49.92, altitude=4000)

kml.save("stredovek_com.kml")

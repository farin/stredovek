#!/usr/bin/env python
# coding: utf-8

import urllib
import re
import time
import cPickle as pickle

import requests
from pyquery import PyQuery as pq

DOC_ROOT = 'http://stredovek.com/'
SOURCES = {
    'hradiste': {
        'url': 'list_directory.php?category=hradiste'
    },
    'hrady': {
        'url': 'list_directory.php?category=hrady'
    },
    'rozhledny': {
        'url': 'list_directory.php?category=rozhledny'
    },
    'jine': {
        'url': 'list_directory.php?category=jineobjekty'
    },
}

re_gps = re.compile(r'GPS:[^\d]*(\d+)[^\d]*(\d+)[^\d]*([\d\.]+)[^\d]*(\d+)[^\d]*(\d+)[^\d]*([\d\.]+)')


def fixurl(url):
    idx = url.rfind('=')
    return url[:idx] + '=' + urllib.quote(url[idx + 1:])


def process_index(html):
    d = pq(html)
    places = []

    for cell in map(d, d("table").eq(1).find("td a")):
        places.append({
            'title': cell.find('font').text(),
            'thumb': cell.find('img').attr('src')[2:],
            'url': fixurl(cell.attr('href')),
        })
    return places


def process_detail(html, place):
    d = pq(html)
    desc = []

    for cell in map(d, d('table').eq(0).find('td').eq(0).find('p')):
        desc.extend(['<p>', cell.text(), '</p>'])
    place['desc'] = ''.join(desc)

    images = []
    for cell in map(d, d('table').eq(1).find('a')):
        thmumb_url = urllib.quote(cell.find('img').attr('src')[2:])
        full_url = urllib.quote(cell.attr('href')[2:])
        images.append([thmumb_url, full_url])
    place['images'] = images

    m = re_gps.search(html)

    place['lat'] = int(m.group(1)) + int(m.group(2)) / 60.0 + float(m.group(3)) / 3600.0
    place['lon'] = int(m.group(4)) + int(m.group(5)) / 60.0 + float(m.group(6)) / 3600.0
    return place

all_places = {}

for (category, source) in SOURCES.items():
    index = requests.get(DOC_ROOT + source['url'])
    index.encoding = 'windows-1250'
    places = process_index(index.text)

    for place in places:
        print place['title']
        time.sleep(0.2)
        detail = requests.get(DOC_ROOT + place['url'])
        if detail.status_code != 200:
            print DOC_ROOT + place['url'], detail.status_code
            continue
        detail.encoding = 'windows-1250'
        process_detail(detail.text, place)

    all_places[category] = places

with open('places.pkl', 'wb') as f:
    pickle.dump(all_places, f)

# import codecs
# with codecs.open("sample.html", "r", 'windows-1250') as f:
#     places = process_index(f.read())
#     print places[:3]

# import codecs
# with codecs.open("sample-detail.html", "r", 'windows-1250') as f:
#     print process_detail(f.read(), {})
